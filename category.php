<?php get_header(); ?>


	<section id="header">
		<div class="wrapper">

			<h1><?php single_cat_title(); ?></h1>

		</div>
	</section>

	<section id="posts">
		<div class="wrapper">

			<?php
				$cat = get_query_var('cat');
				$category = get_category ($cat);
				echo do_shortcode('[ajax_load_more container_type="div" post_type="post" posts_per_page="10" category="'.$category->slug.'" scroll="false" button_label="Load More Articles"]');
			?>

		</div>
	</section>

<?php get_footer(); ?>