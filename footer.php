	<footer>
		<div class="wrapper">

			<div class="logo">
				<a href="<?php the_field('footer_logo_link', 'options'); ?>">
					<img src="<?php $image = get_field('footer_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<div class="description">
				<?php the_field('footer_description', 'options'); ?>
			</div>

			<div class="taglines">
				<?php if(have_rows('footer_taglines', 'options')): while(have_rows('footer_taglines', 'options')): the_row(); ?>
				    <div class="tagline">
				        <h4><?php the_sub_field('tagline'); ?></h4>
				    </div>
				<?php endwhile; endif; ?>
			</div>

			<div class="more">
				<a href="<?php the_field('footer_more_link', 'options'); ?>" rel="external"><?php the_field('footer_more_link_label', 'options'); ?></a>
			</div>

			<div class="clients">
				<h5><?php the_field('footer_client_headline', 'options'); ?></h5>
				
				<?php if(have_rows('footer_client_logos', 'options')): while(have_rows('footer_client_logos', 'options')): the_row(); ?>
				 
				    <div class="logos">
				    	<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    </div>
				<?php endwhile; endif; ?>

			</div>

		</div>
	</footer>

	<?php get_template_part('partials/newsletter-modal'); ?>
	<?php get_template_part('partials/search-modal'); ?>
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

</body>
</html>