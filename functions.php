<?php

/*

    ----------------------------------------------------------------------
    					XX Theme Support
    ----------------------------------------------------------------------

*/

show_admin_bar( false );

function register_my_menu() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_my_menu' );

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');







/*

    ----------------------------------------------------------------------
    					XX Code Cleanup
    ----------------------------------------------------------------------

*/

remove_action( 'wp_head', 'wp_resource_hints', 2 );
remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action( 'wp_head', 'wp_generator');
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}
 
// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );
 
// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );






/*

    ----------------------------------------------------------------------
    					XX Custom Functions
    ----------------------------------------------------------------------

*/

/* Filter <p>'s on <img> and <iframe>' */
function filter_ptags_on_images($content) {
	$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	return preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '\1', $content);
}
add_filter('the_content', 'filter_ptags_on_images');
add_filter('acf/format_value/type=wysiwyg', 'filter_ptags_on_images', 10, 3);


/* List of Tag Slugs */
function entry_tags() {
	$posttags = get_the_tags();
	if ($posttags) {
	  foreach($posttags as $tag) {
	    echo $tag->slug; 
	  }
	}
}

function replace_admin_menu_icons_css() {
    ?>
    <style>

		.dashicons-admin-post:before,
		.dashicons-format-standard:before {
		    content: "\f331";
		}

    </style>
    <?php
}


function search_filter($query) {
  if ( !is_admin() && $query->is_main_query() ) {
    if ($query->is_search) {
      $query->set('post_type', 'post');
    }
  }
}

add_action('pre_get_posts','search_filter');










//Enqueue Ajax Scripts
function enqueue_search_filter_ajax_scripts() {
  wp_register_script('search-filter', get_bloginfo('template_url') . '/js/search-filter.js', '', true );
  wp_localize_script('search-filter', 'ajax_listing_params', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
  wp_enqueue_script('search-filter', get_bloginfo('template_url') . '/js/search-filter.js','','',true);
}
add_action('wp_enqueue_scripts', 'enqueue_search_filter_ajax_scripts');

//Add Ajax Actions
add_action('wp_ajax_search_filter', 'ajax_search_filter');
add_action('wp_ajax_nopriv_search_filter', 'ajax_search_filter');


function ajax_search_filter() {

  $query_data = $_GET;
  $search_term = $query_data['searchTerm'];
  $order_term = $query_data['order'];
  $cat_term = $query_data['categories'];
  $tag_term = $query_data['tags'];



  $search_args = array(
      'post_type' => 'post',
      'posts_per_page' => 150,
      's' => $search_term,
      'category__in' => $cat_term,
      'tag__in' => $tag_term,
      'meta_key' => 'wpb_post_views_count',
      'orderby' => $order_term,
      'order' => 'DESC' 
  );

  $search_loop = new WP_Query($search_args);

  if( $search_loop->have_posts() ): while( $search_loop->have_posts() ): $search_loop->the_post();
    get_template_part('partials/post');
  endwhile;   
  else:
    get_template_part('partials/post-none');
  endif;
  
  wp_reset_postdata();

  die();
}






// Count Post Views
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');


/*

    ----------------------------------------------------------------------
    					XX Advanced Custom Fields
    ----------------------------------------------------------------------

*/

function my_relationship_query( $args, $field, $post_id ) {
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    return $args;
}


// filter for every field
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

if( function_exists('acf_add_options_sub_page') ) {
	acf_add_options_sub_page('Header');
	acf_add_options_sub_page('Footer');
}

function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

	</style>

	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');