<!DOCTYPE html>
<html>
<head>

	<title><?php wp_title(); ?></title>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?> id="<?php echo $post->post_name; ?>">

	<header>
		<div class="wrapper">

			<div id="logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<a href="#" id="toggle" class="no-translate">
				<div class="patty"></div>
			</a>

			<nav>
				<div class="nav-wrapper">

					<div class="category-links">
						<?php $terms = get_field('navigation', 'options'); if( $terms ): ?>

							<?php foreach( $terms as $term ): ?>

								<a href="<?php echo get_term_link( $term ); ?>"><?php echo $term->name; ?></a>

							<?php endforeach; ?>

						<?php endif; ?>
					</div>

					<div class="search">
						<a href="#" class="icon">
							Search
						</a>
					</div>

					<div class="subscribe-btn">
						<a href="#" class="btn">
							Subscribe
						</a>
					</div>

				</div>
			</nav>

		</div>
	</header>