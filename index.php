<?php get_header(); ?>
		
	<?php if ( have_posts() ): ?>

		<section id="posts" class="home">

			<?php $count = 0; while ( have_posts() ): $count++; the_post(); ?>

				<?php if($count == 1): ?>

					<section id="hero">
						<?php get_template_part('partials/post-hero'); ?>
					</section>

					<section id="features">
						<div class="wrapper">

				<?php endif; ?>

				<?php if($count == 2 || $count == 3): ?>

					<?php get_template_part('partials/post-feature'); ?>

				<?php endif; endwhile; ?>

						</div>
					</section>

					<section id="list">
						<div class="wrapper">

							<?php echo do_shortcode('[ajax_load_more container_type="div" post_type="post" posts_per_page="8" offset="3" scroll="false" button_label="Load More Articles"]'); ?>


							<?php get_template_part('partials/sign-up'); ?>

						</div>
					</section>



		</section>

	<?php endif; ?>
	
<?php get_footer(); ?>