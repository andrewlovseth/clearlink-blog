jQuery(function($) {

	//Load posts on page load
	search_get_posts();

	//Find Search Term
	function getSearchTerm() {
		var searchTerm = $('h1 span#search-term').text();	
		return searchTerm;
	}

	//Find Selected Sort Order
	function getSelectedOrder() {
		var order = $('.sort-by a.active').attr('data-sort');	
		return order;
	}

	//Find Selected Categories
	function getSelectedCategories() {
		var categories = [];
		$('.filter-by .cat input:checked').each(function() {
		    categories.push($(this).attr('value'));
		});
		return categories;
	}

	//Find Selected Tags
	function getSelectedTags(){
		var tags = [];
		$('.filter-by .tags input:checked').each(function() {
		    tags.push($(this).attr('value'));
		});
		return tags;
	}

	//On sort/filter change
	$('.sort-by a, .apply a').on('click', function(){
		search_get_posts();
	});
	
	//Main ajax function
	function search_get_posts() {
		var ajax_url = ajax_listing_params.ajax_url;

		$.ajax({
			type: 'GET',
			url: ajax_url,
			data: {
				action: 'search_filter',
				searchTerm: getSearchTerm,
				order: getSelectedOrder,
				categories: getSelectedCategories,
				tags: getSelectedTags
			},
			beforeSend: function () {
				//Show loader here
			},
			success: function(data) {
				//Hide loader here
				$('#response').html(data);
			},
			error: function() {
				$("#response").html('<p>There has been an error</p>');
			}
		});				
	}
	
});