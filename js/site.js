$(document).ready(function() {


	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});


	// Menu Toggle
	$('#toggle').click(function(){
		$('header').toggleClass('open');
		$('nav').slideToggle(300);
		return false;
	});
	

	// Overlay Close
	$('.overlay .close').on('click', function(){
		var modal = $(this).closest('.overlay');
		modal.hide();
		return false;
	});


	// Overlay Close (Outside Modal)
	$('.overlay').on('click', function(event){
		var modal = $(this);

		if(event.target.className == 'overlay') {
			modal.hide();
		}

	});


	// Search Modal
	$('.search .icon').on('click', function(){
		var modal = $('#search-modal');
		modal.css('display', 'flex');
		$('input#search').focus();
		return false;
	});


	// Newsletter Modal
	$('.subscribe-btn .btn').on('click', function(){
		var modal = $('#newsletter-modal');
		modal.css('display', 'flex');
		$('input#field0').focus();
		return false;
	});


	// Sticky Polyfill
	$('aside#share .share-wrapper').fixedsticky();
	$('aside#back-to-top .back-to-top-wrapper').fixedsticky();


	// Back to Top
	$('aside#back-to-top a').smoothScroll( { scrollTarget: '#article-header', offset: -200 } );


	// Sort
	$('.sort-by a').on('click', function(){
		$('.sort-by a').not(this).removeClass('active');
		$(this).addClass('active');
		return false;
	});

	// Tax Filter
	$('.filter-by .tax .dd-btn').on('click', function(){
		$('.filter-by .tax .dd-btn').not(this).siblings('.dropdown').removeClass('show');
		$(this).siblings('.dropdown').toggleClass('show');
		return false;
	});


});



$(window).on('scroll', function () {

	var headerHeight = $('header').height();

    if ($(window).scrollTop() >= headerHeight * 2) {

       $('header').addClass('fixed');

    } else {

       $('header').removeClass('fixed');

    }


});


$(document).on('keyup',function(evt) {
    if (evt.keyCode == 27) {
		var modal = $('.overlay');
		modal.hide();
    }
});


$(window).click(function() {
	$('.filter-by .tax .dd-btn').siblings('.dropdown').removeClass('show');
});

$('.filter-by .tax').click(function(event){
    event.stopPropagation();
});
