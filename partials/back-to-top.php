<aside id="back-to-top">

	<div class="back-to-top-wrapper">

		<a href="#">
			<span class="icon">
				<img src="<?php bloginfo('template_directory') ?>/images/back-to-top.svg" alt="Arrow Up" />
			</span>

			<span class="label">
				Back to top
			</span>
		</a>

	</div>

</aside>