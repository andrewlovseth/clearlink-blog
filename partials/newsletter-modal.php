<div class="overlay" id="newsletter-modal">
	<div class="overlay-wrapper">

		<div class="info">
			<a href="#" class="close"><img src="<?php bloginfo('template_directory') ?>/images/close.svg" alt="Close" /></a>

			<h2><?php the_field('newsletter_headline', 'options'); ?></h2>
			<?php the_field('newsletter_deck', 'options'); ?>

			<div class="form">
				<div>
				  <form method="post" name="CLNewsletterBlog" action="https://s1751497201.t.eloqua.com/e/f2" onsubmit="setTimeout(function(){if(document.querySelector){var s=document.querySelector('form#form28 input[type=submit]');if(s){s.disabled=true;}}},100);return true;" id="form28" class="elq-form" >
				    <input value="CLNewsletterBlog" type="hidden" name="elqFormName"  />
				    <input value="1751497201" type="hidden" name="elqSiteId"  />
				    <input name="elqCampaignId" type="hidden"  />
				    <div id="formElement0" class="sc-view form-design-field sc-static-layout item-padding sc-regular-size" >
				      <div class="field-wrapper" >
				      </div>
				      <div class="individual field-wrapper" >
				        <div class="_100 field-style" >
				          <p class="field-p" >
				            <input id="field0" name="emailAddress" type="text" placeholder="Enter your email" class="field-size-top-medium"  />
				          </p>
				        </div>
				      </div>
				    </div>
				    <div id="formElement1" class="sc-view form-design-field sc-static-layout item-padding sc-regular-size" >
				      <div class="field-wrapper" >
				      </div>
				      <div class="individual field-wrapper" >
				        <div class="_100 field-style" >
				          <p class="field-p" >
				            <input type="submit" value="Sign Up" class="submit-button"  />
				          </p>
				        </div>
				      </div>
				    </div>
				  </form>
				  <script src="https://img04.en25.com/i/livevalidation_standalone.compressed.js" type="text/javascript" >
				  </script>

				  <script type="text/javascript" >
				    var dom0 = document.querySelector('#form28 #field0');
				    var field0 = new LiveValidation(dom0, {
				      validMessage: "", onlyOnBlur: false, wait: 300}
				                                   );
				    field0.add(Validate.Format, {
				      pattern: /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i, failureMessage: "A valid email address is required"}
				              );
				    field0.add(Validate.Presence, {
				      failureMessage:"This field is required"}
				              );
				    function resetSubmitButton(e){
				      var submitButtons = e.target.form.getElementsByClassName('submit-button');
				      for(var i=0;i<submitButtons.length;i++){
				        submitButtons[i].disabled = false;
				      }
				    }
				    function addChangeHandler(elements){
				      for(var i=0; i<elements.length; i++){
				        elements[i].addEventListener('change', resetSubmitButton);
				      }
				    }
				    var form = document.getElementById('form28');
				    addChangeHandler(form.getElementsByTagName('input'));
				    addChangeHandler(form.getElementsByTagName('select'));
				    addChangeHandler(form.getElementsByTagName('textarea'));
				    var nodes = document.querySelectorAll('#form28 input[data-subscription]');
				    if (nodes) {
				      for (i = 0, len = nodes.length; i < len; i++) {
				        var status = nodes[i].dataset ? nodes[i].dataset.subscription : nodes[i].getAttribute('data-subscription');
				        if(status ==='true') {
				          nodes[i].checked = true;
				        }
				      }
				    };
				    var nodes = document.querySelectorAll('#form28 select[data-value]');
				    if (nodes) {
				      for (var i = 0; i < nodes.length; i++) {
				        var node = nodes[i];
				        var selectedValue = node.dataset ? node.dataset.value : node.getAttribute('data-value');
				        if (selectedValue) {
				          for (var j = 0; j < node.options.length; j++) {
				            if(node.options[j].value === selectedValue) {
				              node.options[j].selected = 'selected';
				              break;
				            }
				          }
				        }
				      }
				    }
				  </script>
				</div>


			</div>

		</div>


	</div>
</div>