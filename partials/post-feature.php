<article class="post post-feature">
	<div class="photo cover" style="background-image: url(<?php $image = get_field('featured_image'); echo $image['sizes']['large']; ?>);">
	</div>

	<div class="info">
		<div class="info-wrapper">
			<?php the_category(''); ?>
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<em class="date"><?php the_time('F j, Y'); ?></em>

			<div class="teaser">
				<?php the_field('teaser'); ?><a href="<?php the_permalink(); ?>" class="more">Read more &gt;</a>
			</div>

		</div>
	</div>
</article>