<article class="post post-hero">
	<div class="photo cover" style="background-image: url(<?php $image = get_field('featured_image'); echo $image['sizes']['large']; ?>);">
	</div>

	<div class="info">
		<div class="info-wrapper">
			<?php the_category(''); ?>
			<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

			<div class="teaser">
				<?php the_field('teaser'); ?>
			</div>

			<a href="<?php the_permalink(); ?>" class="btn">Read more</a>
		</div>
	</div>
</article>