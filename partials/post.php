<article class="post post-regular">
	<div class="photo cover" style="background-image: url(<?php $image = get_field('featured_image'); echo $image['sizes']['large']; ?>);">
	</div>

	<div class="info">
		<div class="info-wrapper">

			<div class="meta">
				<?php the_category(''); ?>
				<em class="date"><?php the_time('F j, Y'); ?></em>
			</div>

			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		</div>
	</div>

	<div class="btn-wrapper">
		<a href="<?php the_permalink(); ?>" class="btn">Read more</a>
	</div>
</article>