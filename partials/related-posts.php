<section id="related-posts">
	<div class="wrapper">

		<?php $posts = get_field('related_posts'); if( $posts ): ?>
		
			<h4>You May Also Like...</h4>

			<div class="posts-wrapper">

			    <?php foreach( $posts as $post): setup_postdata($post); ?>
			    
			    	<article class="related-post">

			    		<div class="photo">
			    			<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
			    		</div>

			    		<div class="info">
							<?php the_category(''); ?>
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							<em class="date"><?php the_time('F j, Y'); ?></em>
			    		</div>

				    </article>

			    <?php endforeach; ?>

			</div>

		<?php wp_reset_postdata(); endif; ?>

	</div>
</section>