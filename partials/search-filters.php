<section id="filters">

	<div class="sort-by">
		<h5>Sort by</h5>
		<a href="#" data-sort="date" class="active">Date</a>
		<a href="#" data-sort="meta_value_num">Popularity</a>
	</div>

	<div class="filter-by">
		<h5>Filter by</h5>

		<div class="cat tax">
			<button class="dd-btn">Category</button>

			<div class="dropdown">
				<div class="list">
					<?php $categories = get_categories( array( 'hide_empty' => 0, 'exclude' => 1) ); foreach ( $categories as $category ): ?>
						<div class="item">
							<input type="checkbox" id="<?php echo $category->slug; ?>-item" value="<?php echo $category->term_id; ?>">
							<label for="<?php echo $category->slug; ?>-item"><?php echo $category->name; ?></label>
						</div>
					<?php endforeach; ?>
				</div>

				<div class="apply">
					<a href="#" class="btn">Apply Filter</a>
				</div>
			</div>


		</div>

		<div class="tags tax">

			<button href="#" class="dd-btn">Tag</button>

			<div class="dropdown">
				<div class="list">
					<?php $tags = get_terms('post_tag'); foreach ( $tags as $tag ): ?>
						<div class="item">
							<input type="checkbox" id="<?php echo $tag->slug; ?>-item" value="<?php echo $tag->term_id; ?>">
							<label for="<?php echo $tag->slug; ?>-item"><?php echo $tag->name; ?></label>
						</div>
					<?php endforeach; ?>
				</div>

				<div class="apply">
					<a href="#" class="btn">Apply Filter</a>
				</div>
			</div>

			
		</div>
	</div>

</section>