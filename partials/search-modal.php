<div class="overlay" id="search-modal">
	<div class="overlay-wrapper">

		<div class="info">
			<a href="#" class="close"><img src="<?php bloginfo('template_directory') ?>/images/close.svg" alt="Close" /></a>

			<form action="/" method="get">
			    <input type="text" name="s" id="search" placeholder="Search" value="<?php the_search_query(); ?>" />
			    <input type="image" alt="Search" src="<?php bloginfo('template_directory') ?>/images/search-icon.svg" />
			</form>

		</div>


	</div>
</div>