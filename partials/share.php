<aside id="share">

	<div class="share-wrapper">

		<div class="twitter">
			<a href="https://twitter.com/intent/tweet/?text=<?php echo urlencode(get_the_title()); ?>&url=<?php echo urlencode(get_permalink()); ?>" target="_blank">
				<img src="<?php bloginfo('template_directory') ?>/images/share-twitter.svg" alt="Twitter" />
			</a>
		</div>

		<div class="linkedin">
			<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_permalink()); ?>" target="_blank">
				<img src="<?php bloginfo('template_directory') ?>/images/share-linkedin.svg" alt="LinkedIn" />
			</a>
		</div>

		<div class="facebook">
			<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>" target="_blank">
				<img src="<?php bloginfo('template_directory') ?>/images/share-facebook.svg" alt="Facebook" />
			</a>
		</div>

		<div class="google-plus">
			<a href="https://plus.google.com/share?url=<?php echo urlencode(get_permalink()); ?>" target="_blank">
				<img src="<?php bloginfo('template_directory') ?>/images/share-google-plus.svg" alt="Google +" />
			</a>
		</div>

	</div>

</aside>