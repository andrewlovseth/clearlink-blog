<section id="sign-up">

	<div class="subscribe-headline">
		<h2><?php the_field('sign_up_headline', 'options'); ?></h2>
	</div> 

	<div class="subscribe-deck">
		<p><?php the_field('sign_up_deck', 'options'); ?></p>
	</div> 

	<div class="subscribe-btn">
		<a href="#" class="btn">
			Sign Up
		</a>
	</div> 

</section>