<?php get_header(); ?>


	<section id="header">
		<div class="wrapper">

			<?php get_template_part('partials/search-filters'); ?>

			<h1>Showing all results for "<span id="search-term"><?php echo get_search_query(); ?></span>"</h1>

		</div>
	</section>

	<section id="posts">
		<div class="wrapper">

			<section id="response">

			</section>

		</div>
	</section>

<?php get_footer(); ?>