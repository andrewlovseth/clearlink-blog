<?php get_header(); ?>
		

		<article>
			<div class="wrapper">

				<section id="article-image">
					<img src="<?php $image = get_field('featured_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</section>

				<section id="article-header">
					<?php the_category(''); ?>

					<h1><?php the_title(); ?></h1>

					<div class="byline">
						<div class="photo">
							<?php $post_object = get_field('author'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
								<img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							<?php wp_reset_postdata(); endif; ?>
						</div>

						<div class="info">
							<?php $post_object = get_field('author'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
								<h4 class="author"><?php the_title(); ?></h4>
							<?php wp_reset_postdata(); endif; ?>

							<em class="date">on <?php the_time('F j, Y'); ?></em>
						</div>
					</div>
				</section>

				<section id="article-body">
					<?php get_template_part('partials/share'); ?>

					<div class="story">
						<?php the_content(); ?>
					</div>

					<?php get_template_part('partials/back-to-top'); ?>

				</section>

				<section id="article-footer">

					<?php $posts = get_field('read_more'); if( $posts ): ?>

						<div class="read-more">

							<h4>Read More</h4>

							<ul>

							    <?php foreach( $posts as $post): setup_postdata($post); ?>

							        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

							    <?php endforeach; ?>

							</ul>

						</div>

					<?php wp_reset_postdata(); endif; ?>

					<div class="author">
						<?php $post_object = get_field('author'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
							
							<div class="photo">
								<img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>

							<div class="info">
								<h4><?php the_title(); ?></h4>
								<?php the_content(); ?>
							</div>

						<?php wp_reset_postdata(); endif; ?>
					</div>

					<div class="tags">
						<?php the_tags( ' ', ' ', '' ); ?> 
					</div>


				</section>
		
			</div>
		</article>


		<?php get_template_part('partials/related-posts'); ?>

	
<?php get_footer(); ?>