<?php get_header(); ?>


	<section id="header">
		<div class="wrapper">

			<h1><?php single_tag_title(); ?></h1>

		</div>
	</section>

	<section id="posts">
		<div class="wrapper">

			<?php
				$tag = get_query_var('tag'); 
				echo do_shortcode('[ajax_load_more container_type="div" post_type="post" posts_per_page="10" tag="'.$tag.'" scroll="false" button_label="Load More Articles"]');
			?>
			
		</div>
	</section>

<?php get_footer(); ?>